from caffe2.proto import caffe2_pb2
from caffe2.python import core, workspace
import numpy as np
import model

class Caffe2MNIST(model.Caffe2Model):

	def __init__(self, model_name):
		model.Caffe2Model.__init__(self)
		self.model_name = model_name

	def Run(self, data, blob):

		data = data[np.newaxis, np.newaxis,:,:].astype(np.float32)
		data = data/256

		workspace.FeedBlob(blob, data, device_option=self.device_opts)
		workspace.RunNet(self.model_name, 1)

		output = workspace.FetchBlob("softmax")
		idx = np.argmax(output)
		acc = output[0][idx]

		return idx,acc
