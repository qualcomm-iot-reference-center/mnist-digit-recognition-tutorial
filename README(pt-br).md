# **MNIST: Criando uma CNN para DragonBoard 410C**


Este artigo aborda o treinamento de uma rede CNN utilizando Caffe2 e os procedimentos para executá-la na DragonBoard 410C. Para tal, o MNIST dataset será utilizado para reconhecimento de digitos.

## **MNIST Dataset**

O dataset utilizado para fazer o treinamento e reconhecimento dos dígitos é o MNIST. Esse dataset é composto por imagens de números escritos à mão, contendo dígitos de 0 a 9 com tamanho de 28x28 pixels.
 
 <div align="center">
    <figure>
        <img src="/Tutorial/MNIST.png">
        <figcaption>Exemplo de imagens armazenadas no dataset.</figcaption>
    </figure>
</div> 

## **Ambiente de Treinamento (PC - Linux)**

Para realizar o treinamento da CNN é necessário instalar o Anaconda na máquina de desenvolvimento.

##### - Passo 1: Instalação do anaconda2 para o python 2.7

Realizar download: https://www.anaconda.com/download/#linux
Para outras arquiteturas, verifique as opções no link indicado.

```sh
cd /home/${USER}
wget https://repo.continuum.io/archive/Anaconda2-2018.12-Linux-x86_64.sh
```

Comando para instalação: https://conda.io/docs/user-guide/install/linux.html

```sh
bash Anaconda2-2018.12-Linux-x86_64.sh
```

Confirme a licença e o diretório de instalação (/home/${USER}/anaconda2). Em seguida, teste o utilitário.

```sh
source ~/.bashrc
conda info
```

##### - Passo 2: Instalação do Opencv na máquina de desenvolvimento

```sh
sudo apt-get install libgtk2.0-dev
sudo apt-get install pkg-config
sudo apt-get install python-opencv
```


##### - Passo 3: Instalação do Caffe2 na máquina de desenvolvimento

Realizar o download da biblioteca lmdb:

```sh
sudo apt-get update 
sudo apt-get install liblmdb-dev
```

Realizar o download do pytorch.

```sh
$ cd /home/${USER}
$ git clone --recursive https://github.com/pytorch/pytorch.git && cd pytorch
$ git submodule update --init --recursive
```

No diretório pytorch, execute o script de compilação do caffe2: 

```sh
./scripts/build_local.sh -DUSE_LMDB=ON
cd build
sudo ninja install
```

Por fim, adicione as bibliotecas no conda:

```sh
pip install lmdb
pip install google
pip install matplotlib
pip install numpy==1.15.0
```

Cabe ressaltar que a variável de ambiente PYTHONPATH deve ser atualizada contendo o diretório do caffe2.

```sh
export PYTHONPATH=/home/$USER/pytorch/build:$PYTHONPATH
```

## **Treinamento da Rede na Máquina de Desenvolvimento**

> O tutorial MNIST - Handwriting Recognition possui um notebook com o procedimento para realizar o download do dataset e realizar a construção do modelo. Entre no link abaixo:

> > Notebook MNIST: https://github.com/caffe2/tutorials/blob/master/MNIST.ipynb



O mesmo procedimento encontrado no notebook disponibilizado no tutorial MNIST from Scratch pode ser realizado conforme os passos indicados abaixo.

Durante o procedimento de treinamento será realizado o download do MNIST dataset.

> Caso ocorra algum erro durante essa etapa, realize o download manualmente no link https://download.caffe2.ai/databases/mnist-lmdb.zip.

Antes de executar o script de treinamento, crie um diretório para manter todos os arquivos.

```sh
mkdir -p /home/${USER}/MNIST
cd /home/${USER}/MNIST
git clone https://gitlab.com/qualcomm-iot-reference-center/mnist-digit-recognition-tutorial.git
cd mnist-digit-recognition-tutorial
```

Em seguida, crie os diretórios onde serão realizados o download e a criação dos modelos treinados.

```sh
sh env.sh
```

Por fim, execute o script *mnist_cnn.py* para realizar o treinamento da rede.

```sh
python mnist_cnn.py
```

O resultado inicial da operação deve ser o seguinte:

> Your current folder is: ../caffe2_notebooks
> one or both of the MNIST lmbd dbs not found!!
> Downloading... http://download.caffe2.ai/databases/mnist-lmdb.zip to ../caffe2_notebooks/tutorial_data/mnist
> Completed download and extraction.
> training data folder:../caffe2_notebooks/tutorial_data/mnist
> workspace root folder:../caffe2_notebooks/tutorial_files/tutorial_mnist


Após a execução, os arquivos gerados devem ser copiado para o diretório local: 

```sh
sh update.sh
```


## **Aplicação utilizando MNIST dataset**

A seguir, há um exemplo da utilização do MNIST *dataset*. Considere a imagem abaixo, obtida [neste link](https://www.google.com.br/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwis0J3YkM_eAhVPPJAKHXD_DRkQjRx6BAgBEAU&url=https%3A%2F%2Fwww.etsy.com%2Fes%2Flisting%2F229452638%2Fcasa-de-primitiva-plantilla-3-libro&psig=AOvVaw1o0vq19HzA6tus44XMovSo&ust=1542118189185835)

<div align="center">
    <figure>
        <img src="/Tutorial/numbers.png">
        <figcaption>Imagem que será utilizada para classificação.</figcaption>
    </figure>
</div> 

O script segmenta os números da imagem original gerando uma nova imagem para cada um. As imagens geradas são processadas para servirem de entrada do modelo treinado.

Para executar a aplicação na máquina de desenvolvimento:

```sh
python Caffe2Test.py
```

O resultado de saída é mostrado abaixo:

<div align="center">
    <figure>
        <img src="/Tutorial/numbers2.png">
        <figcaption>Imagem com os digitos segmentados.</figcaption>
    </figure>
</div> 


### **Carregando Modelos**

Como dito anteriormente, os modelos pré-treinados são armazenados em dois arquivos. Para carregar tais arquivos, criou-se a classe Caffe2Model no arquivo model.py. A classe apresenta o método **_LoadPreTrainedModels_** que recebe a localização dos dois arquivos. 

```python
import caffe2.python.predictor.predictor_exporter as pe 
import caffe2.python.predictor.predictor_py_utils as pu 
import caffe2.proto.predictor_consts_pb2 as predictor_consts 
from caffe2.proto import caffe2_pb2 

from caffe2.python import core, workspace 

core.GlobalInit(['caffe2', '--caffe2_log_level=0']) 

class Caffe2Model: 
    def __init__(self): 
        self.device_opts = core.DeviceOption(caffe2_pb2.CPU, 0) 

    def LoadPreTrainedModels(self,init_net_pb,predict_net_pb): 

        self.init_def = caffe2_pb2.NetDef() 
        
        with open(init_net_pb, 'r') as f: 
            self.init_def.ParseFromString(f.read()) 
            self.init_def.device_option.CopyFrom(self.device_opts) 
        
        workspace.RunNetOnce(self.init_def.SerializeToString()) 

        self.net_def = caffe2_pb2.NetDef() 
        
        with open(predict_net_pb, 'r') as f: 
            self.net_def.ParseFromString(f.read()) 
            self.net_def.device_option.CopyFrom(self.device_opts) 
        
        workspace.CreateNet(self.net_def.SerializeToString(), overwrite=True) 

def Run(self, data, blob): 
    pass 
```

### **Executando modelos**

Para executar o modelo é necessário preencher a entrada da rede. Tal operação foi realizada por outra classe, denominada Caffe2MNIST e definida no arquivo *mnist.py*. 

O método *_Run_* converte a imagem para o padrão de entrada da rede e preenche o *_blob_* de entrada. Posteriormente, a rede é executada tendo como saída uma estimativa percentual para cada dígito. Assim sendo, considera-se o índice de maior valor como o dígito reconhecido.

```python
from caffe2.proto import caffe2_pb2 
from caffe2.python import core, workspace 
import numpy as np 
import model 

class Caffe2MNIST(model.Caffe2Model): 

    def __init__(self, model_name): 
        model.Caffe2Model.__init__(self) 
        self.model_name = model_name 
    
    def Run(self, data, blob): 
    
        data = data[np.newaxis, np.newaxis,:,:].astype(np.float32) 
        data = data/256 
        
        workspace.FeedBlob(blob, data, device_option=self.device_opts) 
        workspace.RunNet(self.model_name, 1) 
        
        output = workspace.FetchBlob("softmax") 
        idx = np.argmax(output) 
        acc = output[0][idx] 
        
        return idx,acc 
```

## **Aplicação Final: Executando o modelo na DragonBoard410**

> Essa aplicação foi executada na plataforma de desenvolvimento DragonBoard410c. A imagem do sistema operacional é a linaro-buster-alip-dragonboard-410c-359.img.gz e pode ser obtida neste link: https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/ 

> Versão do kernel:

> Linux qubuntu 4.15.0-36-generic #39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux

> Cabe ressaltar que o sistema utiliza um uma memória externa para expandir a capacidade do rootfs e fornecer uma região de swap. A configuração desse mecanismo é a seguinte: de 4GB de swap e o restante dedicado para o rootfs. 

> Para executar a aplicação na DB410, instale o [Caffe2](https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test).

> Cabe ressaltar que a aplicação buscará os modelos treinados na mesma estrutura de diretórios apresentados. Portanto copie os modelos treinados ou utilize os disponibilizados no repositório.


Certifique-se de que os módulos numpy e opencv estão instalados na placa.
```sh
sudo apt-get install python-opencv
sudo pip install numpy
```

A aplicação faz uso das últimas duas classes apresentadas. Primeiro, a rede é inicializada. Em seguida, a imagem é carregada e segmentada. Depois, para cada dígito segmentado, o método *_Run_* é executando retornando a classificação. 


```sh
cd /home/$USER
git clone https://gitlab.com/qualcomm-iot-reference-center/mnist-digit-recognition-tutorial.git
cd mnist-digit-recognition-tutorial
python Caffe2Test.py
```

> Observação: O resultado obtido pode variar conforme o treinamento da rede.


```python
import numpy as np 
import os 
import cv2 
import sys 
  
sys.path.append(os.getcwd()) 
import model 
import mnist 


print '\n********************************************' 
print 'loading test model' 

current_folder = '../caffe2_notebooks' 
root_folder = os.path.join(current_folder, 'tutorial_files', 'tutorial_mnist') 
init_pb = os.path.join(root_folder,'init_net.pb') 
predict_pb = os.path.join(root_folder,'predict_net.pb') 


mnist_model = mnist.Caffe2MNIST('mnist_deploy') 
mnist_model.LoadPreTrainedModels(init_pb, predict_pb) 


#Load image 
img = cv2.imread('numbers1.jpg',cv2.IMREAD_GRAYSCALE) 
blur = cv2.blur(img,(5,5)) 
ret,im_th = cv2.threshold(blur,150,255,cv2.THRESH_BINARY_INV) 


# Find contours in the filtered image 
_,ctrs, hier = cv2.findContours(im_th.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 

for ctr in ctrs: 

   [x,y,w,h] = cv2.boundingRect(ctr) 
   if (w > 20 and h > 40): 
    cv2.rectangle(img, (x,y),(x+w,y+h), (0, 255, 0), 3) 
    #Get number form original imagem 
    roi = im_th[y:y+h,x:x+w] 
    roi = cv2.resize(roi, (18, 18),interpolation=cv2.INTER_AREA) 

    # Create a black box 
    bbox = np.zeros([28,28]) 

    yi = int(28 / 2) - int(roi.shape[0] / 2) 
    yf = yi + int(roi.shape[0]) 
    xi = int(28 / 2) - int(roi.shape[1] / 2) 
    xf = xi + int(roi.shape[1]) 

    #Copy roi into black box 
    bbox[yi:yf, xi:xf] = roi 

    cv2.imshow('Image {}'.format(str(x)), bbox) 

    #run Caffe2 pre-trained model and get the output class 
    num,acc = mnist_model.Run(bbox,"data") 
    print("Output class: {}, {}".format(num,acc)) 

    cv2.putText(img, str(num), (x+w,y),cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1) 

cv2.imshow("Result Image", img) 

cv2.waitKey(0) 
cv2.destroyAllWindows() 
```

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
