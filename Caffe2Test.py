import numpy as np
import os
import cv2
import sys

sys.path.append(os.getcwd())
import model
import mnist


print '\n********************************************'
print 'loading test model'


root_folder = './'
init_pb = os.path.join(root_folder,'init_net.pb')
predict_pb = os.path.join(root_folder,'predict_net.pb')


mnist_model = mnist.Caffe2MNIST('mnist_deploy')
mnist_model.LoadPreTrainedModels(init_pb, predict_pb)


#Load image
img = cv2.imread('numbers1.jpg',cv2.IMREAD_GRAYSCALE)
blur = cv2.blur(img,(5,5))
ret,im_th = cv2.threshold(blur,150,255,cv2.THRESH_BINARY_INV)

# Find contours in the filtered image
ctrs, hier = cv2.findContours(im_th.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


for ctr in ctrs:

    [x,y,w,h] = cv2.boundingRect(ctr)
    if (w > 20 and h > 40):
	    cv2.rectangle(img, (x,y),(x+w,y+h), (0, 255, 0), 3)
	    #Get number form original imagem
	    roi = im_th[y-2:y+h+2,x-2:x+w+2]
	    roi = cv2.resize(roi, (18, 18),interpolation=cv2.INTER_AREA)

	    # Create a black box
	    bbox = np.zeros([28,28])

	    yi = int(28 / 2) - int(roi.shape[0] / 2)
	    yf = yi + int(roi.shape[0])
	    xi = int(28 / 2) - int(roi.shape[1] / 2)
	    xf = xi + int(roi.shape[1])

	    #Copy roi into black box
	    bbox[yi:yf, xi:xf] = roi

	    #cv2.imshow('Image {}'.format(str(x)), bbox)

	    #run Caffe2 pre-trained model and get the output class
	    num,acc = mnist_model.Run(bbox,"data")
	    print("Output class: {}, {}".format(num,acc))

	    cv2.putText(img, str(num), (x+w,y),cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1)


cv2.imshow("Result Image", img)

cv2.waitKey(0)
cv2.destroyAllWindows()
