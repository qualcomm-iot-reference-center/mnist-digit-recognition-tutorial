import caffe2.python.predictor.predictor_exporter as pe
import caffe2.python.predictor.predictor_py_utils as pu
import caffe2.proto.predictor_consts_pb2 as predictor_consts
from caffe2.proto import caffe2_pb2
from caffe2.python import core, workspace

core.GlobalInit(['caffe2', '--caffe2_log_level=0'])

class Caffe2Model:

	def __init__(self):
		self.device_opts = core.DeviceOption(caffe2_pb2.CPU, 0)
		

	def LoadPreTrainedModels(self,init_net_pb,predict_net_pb):

	    self.init_def = caffe2_pb2.NetDef()
	    with open(init_net_pb, 'r') as f:
		self.init_def.ParseFromString(f.read())
		self.init_def.device_option.CopyFrom(self.device_opts)
		workspace.RunNetOnce(self.init_def.SerializeToString())

	    self.net_def = caffe2_pb2.NetDef()
	    with open(predict_net_pb, 'r') as f:
		self.net_def.ParseFromString(f.read())
		self.net_def.device_option.CopyFrom(self.device_opts)
		workspace.CreateNet(self.net_def.SerializeToString(), overwrite=True)


	def Run(self, data, blob):
		pass


