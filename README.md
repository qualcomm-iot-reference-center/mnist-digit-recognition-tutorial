# MNIST: Creating a CNN for DragonBoard 410C


This article discusses the training of a CNN network using Caffe2 and the procedures for running it on the DragonBoard 410C. For this, the MNIST dataset will be used for digit recognition.

## MNIST Dataset

The dataset used to do training and digits recognition is the MNIST. This dataset is composed of handwritten numbers, containing digits 0 through 9 with a size of 28x28 pixels.

 <div align="center">
    <figure>
        <img src="/Tutorial/MNIST.png">
        <figcaption>Example of images stored in the dataset.</figcaption>
    </figure>
</div>

## Training Environment (PC - Linux)

To perform CNN training, you must install Anaconda on the development machine.

##### - Step 1: Installing anaconda2 for python 2.7

Download: https://www.anaconda.com/download/#linux
For other architectures, check the options in the link provided.

```sh
cd /home/${USER}
wget https://repo.continuum.io/archive/Anaconda2-2018.12-Linux-x86_64.sh
```


Command for installation: https://conda.io/docs/user-guide/install/linux.html

```sh
bash Anaconda2-2018.12-Linux-x86_64.sh
```


Confirm the installation license and directory (/home/${USER}/anaconda2). Then test the utility.

```sh
source ~/.bashrc
conda info
```


##### - Step 2: Installing Opencv on the development machine


```sh
sudo apt-get install libgtk2.0-dev
sudo apt-get install pkg-config
sudo apt-get install python-opencv
```

##### - Step 3: Installing Caffe2 on the development machine

Download the lmdb library:

```sh
sudo apt-get update 
sudo apt-get install liblmdb-dev
```

Download pytorch:

```sh
$ cd /home/${USER}
$ git clone --recursive https://github.com/pytorch/pytorch.git && cd pytorch
$ git submodule update --init --recursive
```

In the pytorch directory, run the caffe2 compile script:

```sh
./scripts/build_local.sh -DUSE_LMDB=ON
cd build
sudo ninja install
```

Finally, add the libraries in the conda:

```sh
pip install lmdb
pip install google
pip install matplotlib
pip install numpy==1.15.0
```

It should be noted that the PYTHONPATH environment variable must be updated containing the caffe2 directory.

```sh
export PYTHONPATH=/home/$USER/pytorch/build:$PYTHONPATH
```

## Network Training on the Development Machine

> The MNIST - Handwriting Recognition tutorial has a notebook with the procedure to download the dataset and build the model. Enter the link below:
> > MNIST Notebook: https://github.com/caffe2/tutorials/blob/master/MNIST.ipynb


The same procedure found in the notebook provided in the MNIST from Scratch tutorial can be performed according to the steps indicated below.

During the training procedure, the MNIST dataset will be downloaded.

> If an error occurs during this step, manually download the link at https://download.caffe2.ai/databases/mnist-lmdb.zip.

Before running the training script, create a directory to keep all the files.

```sh
mkdir -p /home/${USER}/MNIST
cd /home/${USER}/MNIST
git clone https://gitlab.com/qualcomm-iot-reference-center/mnist-digit-recognition-tutorial.git
cd mnist-digit-recognition-tutorial
```

Next, create the directories where you will download and create your trained models.


```sh
sh env.sh
```

Finally, run the *mnist_cnn.py* script to perform network training.

```sh
python mnist_cnn.py
```

The initial result of the operation should be as follows:

> Your current folder is: ../caffe2_notebooks
> one or both of the MNIST lmbd dbs not found!!
> Downloading... http://download.caffe2.ai/databases/mnist-lmdb.zip to ../caffe2_notebooks/tutorial_data/mnist
> Completed download and extraction.
> training data folder:../caffe2_notebooks/tutorial_data/mnist
> workspace root folder:../caffe2_notebooks/tutorial_files/tutorial_mnist


After execution, the generated files should be copied to the local directory:

```sh
sh update.sh
```


## Application using MNIST dataset

The following is an example of using MNIST dataset. Consider the image below, obtained [at this link](https://www.google.com.br/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwis0J3YkM_eAhVPPJAKHXD_DRkQjRx6BAgBEAU&url=https%3A%2F%2Fwww.etsy.com%2Fes%2Flisting%2F229452638%2Fcasa-de-primitiva-plantilla-3-libro&psig=AOvVaw1o0vq19HzA6tus44XMovSo&ust=1542118189185835).

<div align="center">
    <figure>
        <img src="/Tutorial/numbers.png">
        <figcaption>Image that will be used for classification.</figcaption>
    </figure>
</div>

The script segments the numbers of the original image, generating a new image for each one. The generated images are processed to serve as inputs to the trained model.

To run the application on the development machine:

```sh
python Caffe2Test.py
```


The output result is shown below:

<div align="center">
    <figure>
        <img src="/Tutorial/numbers2.png">
        <figcaption>Image with the segmented digits.</figcaption>
    </figure>
</div>

### Loading Models

As previously stated, pre-trained models are stored in two files. The Caffe2Model class in the model.py file loads such files. The class presents the method **_LoadPreTrainedModels_** that receives the location of the two files.

```python
import caffe2.python.predictor.predictor_exporter as pe 
import caffe2.python.predictor.predictor_py_utils as pu 
import caffe2.proto.predictor_consts_pb2 as predictor_consts 
from caffe2.proto import caffe2_pb2 

from caffe2.python import core, workspace 

core.GlobalInit(['caffe2', '--caffe2_log_level=0']) 

class Caffe2Model: 
    def __init__(self): 
        self.device_opts = core.DeviceOption(caffe2_pb2.CPU, 0) 

    def LoadPreTrainedModels(self,init_net_pb,predict_net_pb): 

        self.init_def = caffe2_pb2.NetDef() 
        
        with open(init_net_pb, 'r') as f: 
            self.init_def.ParseFromString(f.read()) 
            self.init_def.device_option.CopyFrom(self.device_opts) 
        
        workspace.RunNetOnce(self.init_def.SerializeToString()) 

        self.net_def = caffe2_pb2.NetDef() 
        
        with open(predict_net_pb, 'r') as f: 
            self.net_def.ParseFromString(f.read()) 
            self.net_def.device_option.CopyFrom(self.device_opts) 
        
        workspace.CreateNet(self.net_def.SerializeToString(), overwrite=True) 

def Run(self, data, blob): 
    pass 
```

### Running the Application

To run the model you need to fill in the network entry. Another class performs this operation, named Caffe2MNIST and defined in the *mnist.py* file.

The *_Run_* method converts the image to the input pattern of the network and populates the input *_blob_*. Subsequently, the network is executed having as an output a percentage estimate for each digit. Therefore, the index of the highest value is considered as the digit recognized.


```python
from caffe2.proto import caffe2_pb2 
from caffe2.python import core, workspace 
import numpy as np 
import model 

class Caffe2MNIST(model.Caffe2Model): 

    def __init__(self, model_name): 
        model.Caffe2Model.__init__(self) 
        self.model_name = model_name 
    
    def Run(self, data, blob): 
    
        data = data[np.newaxis, np.newaxis,:,:].astype(np.float32) 
        data = data/256 
        
        workspace.FeedBlob(blob, data, device_option=self.device_opts) 
        workspace.RunNet(self.model_name, 1) 
        
        output = workspace.FetchBlob("softmax") 
        idx = np.argmax(output) 
        acc = output[0][idx] 
        
        return idx,acc 
```

## Final Application: Running the application on DragonBoard410

> This application was run on the DragonBoard410c development platform. The image of the operating system is linaro-buster-alip-dragonboard-410c-359.img.gz and can be obtained at this link: https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/ 

> Kernel version:

> Linux qubuntu 4.15.0-36-generic # 39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU / Linux

> It should be noted that the system uses an external memory to expand the capacity of the rootfs and provide a swap region. The configuration of this mechanism is as follows: 4GB swap and the rest dedicated to the rootfs.

> To run the application on DB410, install [Caffe2](https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test).

> It should be noted that the application will look for the models trained in the same structure of directories presented. Therefore copy the trained templates or use those available in the repository.


Make sure that the numpy and opencv modules are installed on the board.

```sh
sudo apt-get install python-opencv
sudo pip install numpy
```

The application makes use of the last two classes presented. First, the network initializes. The image is then loaded and segmented. Then, for each segmented digit, the *_Run_* method is executed by returning the sort.

```sh
cd /home/$USER
git clone https://gitlab.com/qualcomm-iot-reference-center/mnist-digit-recognition-tutorial.git
cd mnist-digit-recognition-tutorial
python Caffe2Test.py
```


> Note: The result obtained may vary according to the training of the network.

```python
import numpy as np 
import os 
import cv2 
import sys 
  
sys.path.append(os.getcwd()) 
import model 
import mnist 


print '\n********************************************' 
print 'loading test model' 

current_folder = '../caffe2_notebooks' 
root_folder = os.path.join(current_folder, 'tutorial_files', 'tutorial_mnist') 
init_pb = os.path.join(root_folder,'init_net.pb') 
predict_pb = os.path.join(root_folder,'predict_net.pb') 


mnist_model = mnist.Caffe2MNIST('mnist_deploy') 
mnist_model.LoadPreTrainedModels(init_pb, predict_pb) 


#Load image 
img = cv2.imread('numbers1.jpg',cv2.IMREAD_GRAYSCALE) 
blur = cv2.blur(img,(5,5)) 
ret,im_th = cv2.threshold(blur,150,255,cv2.THRESH_BINARY_INV) 


# Find contours in the filtered image 
_,ctrs, hier = cv2.findContours(im_th.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 

for ctr in ctrs: 

   [x,y,w,h] = cv2.boundingRect(ctr) 
   if (w > 20 and h > 40): 
    cv2.rectangle(img, (x,y),(x+w,y+h), (0, 255, 0), 3) 
    #Get number form original imagem 
    roi = im_th[y:y+h,x:x+w] 
    roi = cv2.resize(roi, (18, 18),interpolation=cv2.INTER_AREA) 

    # Create a black box 
    bbox = np.zeros([28,28]) 

    yi = int(28 / 2) - int(roi.shape[0] / 2) 
    yf = yi + int(roi.shape[0]) 
    xi = int(28 / 2) - int(roi.shape[1] / 2) 
    xf = xi + int(roi.shape[1]) 

    #Copy roi into black box 
    bbox[yi:yf, xi:xf] = roi 

    cv2.imshow('Image {}'.format(str(x)), bbox) 

    #run Caffe2 pre-trained model and get the output class 
    num,acc = mnist_model.Run(bbox,"data") 
    print("Output class: {}, {}".format(num,acc)) 

    cv2.putText(img, str(num), (x+w,y),cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 1) 

cv2.imshow("Result Image", img) 

cv2.waitKey(0) 
cv2.destroyAllWindows() 
```